# -*- coding: utf-8 -*-

import yaml


class SchemaError(Exception):
    pass

class Generator(object):
    _data_table_template =\
    '''
CREATE TABLE "{table_name}"(
  "{table_name}_id" SERIAL PRIMARY KEY,
  {columns},
  "{table_name}_created" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER),
  "{table_name}_modified" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER)
  );
    '''
    _relation_table_template =\
    '''
CREATE TABLE "{first_entity}_{second_entity}"(
    "{first_entity}_id" INTEGER NOT NULL,
    "{second_entity}_id" INTEGER NOT NULL,
    PRIMARY KEY ("{first_entity}_id", "{second_entity}_id")
  );
    '''

    _time_handler_template =\
    '''
CREATE OR REPLACE FUNCTION update_{0}_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.{0}_modified = cast(extract(epoch from now()) as integer);
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER "update_{0}_modtime" BEFORE UPDATE ON "{0}"
FOR EACH ROW EXECUTE PROCEDURE  update_{0}_column();
    '''

    _add_foreign_key_template =\
    '''
ALTER TABLE "{child}" ADD "{parent}_id" INTEGER NOT NULL,
    ADD CONSTRAINT "fk_{child}_{parent}_id" FOREIGN KEY ("{parent}_id")
    REFERENCES "{parent}" ("{parent}_id");
    '''

    def __init__(self, schema='schema.yaml'):
        self.load(schema)

    def load(self, new_schema):
        with open(new_schema, 'r') as schema_file:
            self._schema = yaml.load(schema_file)

    def upload(self, db_name = 'my_db'):
        with open(db_name + '.sql', 'w') as out:
            for table in self._schema:
                self._process_data_table(out, table)
            for table in self._schema:
                self._process_relations(out, table)
                self._process_time_handlers(out, table)

    def _process_data_table(self,out, table_name):
        if 'fields' in self._schema[table_name]:
            fields = self._schema[table_name]['fields']
            columns = ', \n'.join(['    "{}_{}" {}'.format(table_name.lower(), x, fields[x]) for x in fields.keys()])
            out.write(self._data_table_template.format(table_name=table_name.lower(), columns=columns))


    def _process_relations(self, out, table_name):
        if 'relations' in self._schema[table_name]:
            relations = self._schema[table_name]['relations']
            for relation in relations:
                if relations[relation] == 'one':
                    if table_name in self._schema[relation]['relations'] and\
                            self._schema[relation]['relations'][table_name] == 'many':
                        out.write(self._add_foreign_key_template.format(child=table_name.lower(),
                                                                               parent=relation.lower()))
                    else:
                        raise SchemaError
                elif relations[relation] == 'many':
                    if table_name in self._schema[relation]['relations'] and\
                            self._schema[relation]['relations'][table_name] == 'many':
                        if relation > table_name:
                            out.write(self._relation_table_template.format(first_entity=table_name.lower(),
                                                                             second_entity=relation.lower()))
                        else:
                            pass
                    else:
                        pass
                else:
                    raise SchemaError

    def _process_time_handlers(self, out, table_name):
        out.write(self._time_handler_template.format(table_name.lower()))