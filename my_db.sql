
CREATE TABLE "category"(
  "category_id" SERIAL PRIMARY KEY,
      "category_title" varchar(50),
  "category_created" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER),
  "category_modified" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER)
  );

CREATE TABLE "article"(
  "article_id" SERIAL PRIMARY KEY,
      "article_text" text, 
    "article_title" varchar(50),
  "article_created" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER),
  "article_modified" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER)
  );

CREATE TABLE "tag"(
  "tag_id" SERIAL PRIMARY KEY,
      "tag_value" varchar(50),
  "tag_created" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER),
  "tag_modified" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER)
  );

CREATE OR REPLACE FUNCTION update_category_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.category_modified = cast(extract(epoch from now()) as integer);
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER "update_category_modtime" BEFORE UPDATE ON "category"
FOR EACH ROW EXECUTE PROCEDURE  update_category_column();

ALTER TABLE "article" ADD "category_id" INTEGER NOT NULL,
    ADD CONSTRAINT "fk_article_category_id" FOREIGN KEY ("category_id")
    REFERENCES "category" ("category_id");

CREATE TABLE "article_tag"(
    "article_id" INTEGER NOT NULL,
    "tag_id" INTEGER NOT NULL,
    PRIMARY KEY ("article_id", "tag_id")
  );

CREATE OR REPLACE FUNCTION update_article_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.article_modified = cast(extract(epoch from now()) as integer);
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER "update_article_modtime" BEFORE UPDATE ON "article"
FOR EACH ROW EXECUTE PROCEDURE  update_article_column();

CREATE OR REPLACE FUNCTION update_tag_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.tag_modified = cast(extract(epoch from now()) as integer);
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER "update_tag_modtime" BEFORE UPDATE ON "tag"
FOR EACH ROW EXECUTE PROCEDURE  update_tag_column();
